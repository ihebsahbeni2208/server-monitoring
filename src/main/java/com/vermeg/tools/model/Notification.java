package com.vermeg.tools.model;

import com.vermeg.tools.type.NotificationType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "notification_id")
    @Getter
    private Long id;

    @Getter
    @Setter
    private LocalDateTime creationDate;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @Getter
    @Setter
    private User user;


    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
    private NotificationType notificationType;

    @Getter
    @Setter
    private String notificationMssg;
}

