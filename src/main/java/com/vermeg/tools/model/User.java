package com.vermeg.tools.model;


import com.vermeg.tools.annotaions.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "user")
@Builder
@AllArgsConstructor
public class User {

    public static final String ADMIN = "ADMIN";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    @Getter
    @Setter
    private Long id;

    @Column(name = "email")
    @Email(message = "*Please provide a valid Email")
    @NotEmpty(message = "*Please provide an Email")
    @Getter
    @Setter
    private String email;

    @Column(name = "user_name")
    @NotEmpty(message = "*Please provide your username")
    @Getter
    @Setter
    private String userName;

    @Column(name = "password")
    @ValidPassword
    @NotEmpty(message = "*Please provide your password")
    @Getter
    @Setter
    private String password;

    @Column(name = "first_name")
    @NotEmpty(message = "*Please provide your first name")
    @Getter
    @Setter
    private String firstName;

    @Column(name = "last_name")
    @NotEmpty(message = "*Please provide your last name")
    @Getter
    @Setter
    private String lastName;

    @Column(name = "active")
    @Getter
    @Setter
    private int active;

    @Getter
    @Setter
    private LocalDateTime creationDate;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    @Getter
    @Setter
    private Set<Role> roles;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter
    @Setter
    private List<Server> servers;

    @OneToMany(mappedBy = "user" , cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Notification> notifications;

    public User() {
        this.servers = new ArrayList<>();
        this.notifications = new ArrayList<>();
    }

    public void addServer(Server server) {
        if (!servers.contains(server)) {
            servers.add(server);
            server.setUser(this);
        }
    }

    public void removeServer(Server server) {
        server.setUser(null);
        this.servers.remove(active);
    }

    public void addNotification(Notification notification) {
        if (!notifications.contains(notification)) {
            notifications.add(notification);
            notification.setUser(this);
        }
    }

    public void removeNotification(Notification notification) {
        notification.setUser(null);
        this.notifications.remove(active);
    }

    public boolean isAdmin() {
        return this.roles.stream().anyMatch(r -> r.getName().equals(ADMIN));
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }


}
