package com.vermeg.tools.model;

import com.vermeg.tools.annotaions.ValidPassword;
import com.vermeg.tools.data.ServerData;
import com.vermeg.tools.type.ConnectionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
@Table ( name = "server" )
@Builder
@AllArgsConstructor

public class Server {
    public Server() {
    }

    private static final String host =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    @Id
    @GeneratedValue ( strategy = GenerationType.IDENTITY )
    @Column ( name = "server_id" )
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @NotEmpty ( message = "*Please provide a server name" )
    private String name;

    @Getter
    @Setter
    @Enumerated ( EnumType.STRING )
    private ConnectionType connectionType;

    @Getter
    @Setter
    @NotEmpty ( message = "*Please provide a username" )
    private String userName;

    @Getter
    @Setter
    @ValidPassword
    @NotEmpty ( message = "*Please provide your password" )
    private String password;

    @Getter
    @Setter
    @OneToMany ( mappedBy = "server", cascade = CascadeType.ALL )
    private List<Monitoring> monitorings; //Relation one to many with Server entity

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn ( name = "user_id", nullable = false )
    private User user;


    public ServerData toData() {
        ServerData.ServerDataBuilder serverDataBuilder = ServerData.builder();
        serverDataBuilder.id(this.id)
                .name(name)
                .name(name)
                .connectionType(connectionType.name())
                .userName(userName)
                .password(password);
        return serverDataBuilder.build();

    }

}

