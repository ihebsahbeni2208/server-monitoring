package com.vermeg.tools.model;

import com.vermeg.tools.type.MessageType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
public class Monitoring {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "monitoring_id")
    @Getter
    private long id;

    @Getter
    @Setter
    private String message;

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    private MessageType messageType;

    @Getter
    @Setter
    private Date time;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "server_id", nullable = false)
    private Server server;
}
