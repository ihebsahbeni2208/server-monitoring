package com.vermeg.tools.type;

public enum MessageType {
    ERROR, INFO, WARNING
}
