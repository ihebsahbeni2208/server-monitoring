package com.vermeg.tools.type;

public enum ConnectionType {
    SSH, FTP, TCP
}
