package com.vermeg.tools.type;

public enum NotificationType {
    ERROR, INFO, WARNING
}
