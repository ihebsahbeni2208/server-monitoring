package com.vermeg.tools.type;

public enum RoleName {
    ROLE_USER("USER"),
    ROLE_ADMIN("ADMIN");

    public String value;

    RoleName(String value){
        this.value = value;
    }
}