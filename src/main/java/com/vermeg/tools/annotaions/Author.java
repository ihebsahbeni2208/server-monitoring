package com.vermeg.tools.annotaions;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention (RUNTIME)
    @Constraint (validatedBy = AuthorValidator.class )
    @Documented
    public @interface Author {

        String message() default "Author is not allowed.";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};

    }

