package com.vermeg.tools.annotaions;

import com.vermeg.tools.type.ConnectionType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.stream.Collectors;

public class AuthorValidator implements ConstraintValidator<Author, String> {
    ConnectionType[] authors = ConnectionType.values();

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return Arrays.stream(authors).map(u->u.name()).collect(Collectors.toList()).contains(value);
    }
}
