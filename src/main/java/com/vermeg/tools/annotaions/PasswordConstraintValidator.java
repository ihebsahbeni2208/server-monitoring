package com.vermeg.tools.annotaions;

import com.vermeg.tools.annotaions.ValidPassword;
import org.passay.*;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {
   public void initialize(ValidPassword constraint) {
   }
   @Override
   public boolean isValid(String password, ConstraintValidatorContext context) {

      PasswordValidator validator = new PasswordValidator(Arrays.asList(
              // at least 8 characters
              new LengthRule(8, 512),

              // at least one upper-case character
              new UppercaseCharacterRule(1),

              // at least one lower-case character
              new LowercaseCharacterRule(1),

              // at least one digit character
              new DigitCharacterRule(1),

              // at least one symbol (special character)
              new SpecialCharacterRule(1),

              // no whitespace
              new WhitespaceRule()

      ));
      RuleResult result = validator.validate(new PasswordData(password));
      if (result.isValid()) {
         return true;
      }
      List<String> messages = validator.getMessages(result);

      String messageTemplate = messages.stream()
              .collect(Collectors.joining("\n"));
      context.buildConstraintViolationWithTemplate(messageTemplate)
              .addConstraintViolation()
              .disableDefaultConstraintViolation();
      return false;


}}
