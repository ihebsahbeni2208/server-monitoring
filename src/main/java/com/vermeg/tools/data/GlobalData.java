package com.vermeg.tools.data;

import com.vermeg.tools.model.User;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class GlobalData {

    private String currentPage;
    private User currentUser;
}
