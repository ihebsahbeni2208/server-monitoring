package com.vermeg.tools.data;

import com.vermeg.tools.annotaions.Author;
import com.vermeg.tools.annotaions.ValidPassword;
import com.vermeg.tools.model.Server;
import com.vermeg.tools.type.ConnectionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


@Data
@Builder
@AllArgsConstructor
public class ServerData implements Serializable {

    private Long id;
    @NotEmpty ( message = "*Please provide a server name" )
    private String name;
    @Author
    private String connectionType;

    public ServerData() {
    }

    @NotEmpty ( message = "*Please provide a username" )
    private String userName;
    @ValidPassword
    private String password;

    public Server toServer(Server checkServer) {
        if (checkServer == null) {
            checkServer = new Server();
        }

        checkServer.setName(name);
        checkServer.setConnectionType(ConnectionType.valueOf(connectionType));
        checkServer.setUserName(userName);
        checkServer.setPassword(password);
        return checkServer;
    }
}
