package com.vermeg.tools.service.impl;

import com.vermeg.tools.data.ServerData;
import com.vermeg.tools.model.Server;
import com.vermeg.tools.model.User;
import com.vermeg.tools.repository.ServerRepository;
import com.vermeg.tools.repository.UserRepository;
import com.vermeg.tools.service.ServerService;
import com.vermeg.tools.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service ( "serverService" )
public class ServerServiceImpl implements ServerService {

    @Autowired
    private ServerRepository serverRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;


    @Override
    public Page<Server> getAllServers(Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        Page<Server> pagedResult = serverRepository.findAll(paging);
        if (pagedResult.hasContent()) {
            return pagedResult;
        } else {
            return Page.empty();
        }
    }

    @Override
    public void saveServer(ServerData serverData) {
        User user = userService.getConnectedUser();
        Optional<Server> checkServerOptional = serverRepository.findById(serverData.getId() != null ? serverData.getId() : -1);
        if (checkServerOptional.isPresent()) {
            Server checkServer = checkServerOptional.get();
            serverRepository.save(serverData.toServer(checkServer));
        } else {
            Server server = serverData.toServer(null);
            user.addServer(server);
            userRepository.save(user);
        }
    }

    @Override
    @Transactional
    public void deleteServer(Long id) {
        Optional<Server> server = serverRepository.findById(id);
        if (server.isPresent()) {
            serverRepository.delete(server.get());
        }
    }

    @Override
    public Optional<Server> findServerById(long id) {
        return serverRepository.findById(id);
    }
}

