package com.vermeg.tools.service.impl;

import com.vermeg.tools.exception.AppException;
import com.vermeg.tools.model.Role;
import com.vermeg.tools.model.User;
import com.vermeg.tools.repository.RoleRepository;
import com.vermeg.tools.repository.UserRepository;
import com.vermeg.tools.service.UserService;
import com.vermeg.tools.type.RoleName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;


@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User getConnectedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth != null ? findUserByEmail(auth.getName()) : null;
    }

    @Override
    public boolean isUserAuthenticated() {
        return getConnectedUser() != null;
    }

    @Override
    public Optional<User> findUserById(long id) {
        return userRepository.findById(id);
    }

    @Override
    public void saveUser(User user) {
        Optional<User> checkUser = userRepository.findById(user.getId() != null ? user.getId() : -1);
        if (checkUser.isPresent()) {
            user.setRoles(checkUser.get().getRoles());
            user.setServers(checkUser.get().getServers());
            userRepository.save(user);
        } else {
            if (!userRepository.existsByUserName(user.getUserName()) && !userRepository.existsByEmail(user.getEmail())) {
                user.setPassword(passwordEncoder.encode(user.getPassword()));

                Role userRole = roleRepository.findByName(RoleName.ROLE_USER.value)
                        .orElseThrow(() -> new AppException("User Role not set."));
                user.setRoles(Collections.singleton(userRole));
                user.setActive(1);
                userRepository.save(user);
            } else {
                throw new DataIntegrityViolationException("Email or UserName already exists");
            }
        }
    }

    @Override
    @Transactional
    public void deleteUser(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            userRepository.delete(user.get());
        }
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Page<User> getAllUsers(Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        Page<User> pagedResult = userRepository.findAll(paging);
        if (pagedResult.hasContent()) {
            return pagedResult;
        } else {
            return Page.empty();
        }
    }

}