package com.vermeg.tools.service;


import com.vermeg.tools.model.User;
import org.springframework.data.domain.Page;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface UserService {
    default User findUserByEmail(String email) {
        return null;
    }

    User getConnectedUser();

    default boolean isUserAuthenticated() {
        return false;
    }

    Optional<User> findUserById(long id);

    void saveUser(User user);

    void deleteUser(Long id);

    List<User> getAllUsers();

    Page<User> getAllUsers(Integer pageNo, Integer pageSize, String sortBy);
}
