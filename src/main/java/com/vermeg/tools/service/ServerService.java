package com.vermeg.tools.service;

import com.vermeg.tools.data.ServerData;
import com.vermeg.tools.model.Server;
import com.vermeg.tools.model.User;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface ServerService {
    Page<Server> getAllServers(Integer pageNo, Integer pageSize, String sortBy);

    void saveServer(ServerData serverData);
    public void deleteServer(Long id);

    Optional<Server> findServerById(long id);
}
