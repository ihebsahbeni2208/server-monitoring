package com.vermeg.tools.repository;

import com.vermeg.tools.model.Server;
import com.vermeg.tools.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository ()
public interface ServerRepository extends JpaRepository<Server, Long>, PagingAndSortingRepository<Server, Long>, CrudRepository<Server, Long> {

    boolean existsByName(String name);
    Optional<Server> findByName(String name);

}
