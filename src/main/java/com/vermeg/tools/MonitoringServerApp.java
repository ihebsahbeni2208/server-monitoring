package com.vermeg.tools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.Async;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
@Async
public class MonitoringServerApp extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(MonitoringServerApp.class, args);
    }

    @PostConstruct
    void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MonitoringServerApp.class);
    }

}
