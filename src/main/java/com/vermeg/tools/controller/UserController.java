package com.vermeg.tools.controller;


import com.vermeg.tools.model.User;
import com.vermeg.tools.service.UserService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.Valid;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.util.Optional;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Controller
@Log4j
public class UserController {

    @Autowired
    private UserService userService;

    @Value(value = "${app.error.user.notfound}")
    private String userNotFoundMssg;


    @GetMapping(value = "/404")
    private String getError()
    {
        return "errors/error404";
    }

    @GetMapping(value = "/500")
    private String getError1()
    {
        return "errors/error500";
    }

    @GetMapping(value = "/user/add")
    private ModelAndView getAddUserView() {
        ModelAndView modelAndView = new ModelAndView("add-user");
        modelAndView.addObject("action", "add");
        modelAndView.addObject("user", new User());
        return modelAndView;
    }

    @PostMapping(value = {"/user/add"})
    private String saveUser(Model model, @Valid User user, BindingResult bindingResult, RedirectAttributes redirectAttrs) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("action", "add");
            return "add-user";
        }
        try {
            userService.saveUser(user);
            model.addAttribute("action", "userAdded");

        } catch (DataIntegrityViolationException e) {
            bindingResult.reject("email.exists", "Email or UserName already exists");
            model.addAttribute("action", "add");
            return "add-user";
        }
        redirectAttrs.addFlashAttribute("isCreated", true);
        return "redirect:/users";
    }

    @GetMapping(value = "/users/{id}/edit")
    private ModelAndView getEditUserView(@PathVariable(name = "id") long id) {
        ModelAndView modelAndView = new ModelAndView("add-user");
        Optional<User> user = userService.findUserById(id);
        if (user.isPresent()) {
            modelAndView.addObject("user", user.get());
        } else {
            modelAndView.addObject("error", userNotFoundMssg);
        }
        modelAndView.addObject("action", "edit");
        return modelAndView;
    }


    @PostMapping(value = "/user/edit")
    private String editUser(Model model, @Valid User user, BindingResult bindingResult, RedirectAttributes redirectAttrs) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("action", "edit");

            return "add-user";
        }
        try {
            userService.saveUser(user);
            model.addAttribute("action", "userEdited");
        } catch (DataIntegrityViolationException e) {
           System.out.println("voila");
            bindingResult.reject("email.exists", "Email or UserName already exists");
            model.addAttribute("action", "edit");
            return "add-user";
        }
        redirectAttrs.addFlashAttribute("isEdited", true);
        return "redirect:/users";
    }

    @PostMapping(value = "/user/delete")
    private String deleteUser(Long id, RedirectAttributes redirectAttrs) {
        userService.deleteUser(id);
        redirectAttrs.addFlashAttribute("isDeleted", true);
        return "redirect:/users";
    }
}
