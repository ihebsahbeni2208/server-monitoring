package com.vermeg.tools.controller;

import com.vermeg.tools.data.GlobalData;
import com.vermeg.tools.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalControllerAdvice {

    @Autowired
    private UserService userService;

    @Autowired
    private GlobalData globalData;

    @ModelAttribute("globaldata")
    public GlobalData getConnected(HttpServletRequest request) {
        globalData.setCurrentUser(userService.getConnectedUser());
        globalData.setCurrentPage(request.getRequestURI());
        return globalData;
    }
}