package com.vermeg.tools.controller;

import com.vermeg.tools.data.ServerData;
import com.vermeg.tools.model.Server;
import com.vermeg.tools.service.ServerService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@Log4j

public class ServerController {
    @Autowired
    private ServerService serverService;



    /*@Value (value = "${app.error.server.notfound}")
    private String serverNotFoundMssg;*/


    @GetMapping ( value = "/server/add" )
    private ModelAndView getAddServerView() {
        ModelAndView modelAndView = new ModelAndView("add-server");
        modelAndView.addObject("action", "add");
        modelAndView.addObject("serverData", new ServerData());
        return modelAndView;
    }


    @PostMapping ( value = {"/server/add"} )
    private String saveServer(Model model, @Valid() ServerData serverData, BindingResult bindingResult, RedirectAttributes redirectAttrs) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("action", "add");
            return "add-server";
        }
        try {
            serverService.saveServer(serverData);
        } catch (DataIntegrityViolationException e) {
            bindingResult.reject("name.exists", "UserName already exists");
            model.addAttribute("action", "add");
            return "add-server";
        }
        redirectAttrs.addFlashAttribute("isCreated", true);
        return "redirect:/servers";
    }


    @PostMapping ( value = "/server/delete" )
    private String deleteServer(Long id, RedirectAttributes redirectAttrs) {
        serverService.deleteServer(id);
        redirectAttrs.addFlashAttribute("isDeleted", true);
        return "redirect:/servers";
    }

    @GetMapping ( value = "/servers/{id}/edit" )
    private ModelAndView getEditServerView(@PathVariable ( name = "id" ) long id) {
        ModelAndView modelAndView = new ModelAndView("add-server");
        Optional<Server> server = serverService.findServerById(id);
        if (server.isPresent()) {
            modelAndView.addObject("serverData", server.get().toData());
        } else {
            modelAndView.addObject("error", "server not found");
        }
        modelAndView.addObject("action", "edit");
        return modelAndView;
    }

    @PostMapping ( value = "/server/edit" )
    private String editServer(Model model, @Valid ServerData serverData, BindingResult bindingResult, RedirectAttributes redirectAttrs) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("action", "edit");
            return "add-server";
        }
        try {
            serverService.saveServer(serverData);
        } catch (DataIntegrityViolationException e) {
            bindingResult.reject("email.exists", "Email or UserName already exists");
            model.addAttribute("action", "edit");
            return "add-server";
        }
        redirectAttrs.addFlashAttribute("isEdited", true);
        return "redirect:/servers";
    }
}
