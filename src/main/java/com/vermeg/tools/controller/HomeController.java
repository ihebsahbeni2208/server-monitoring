package com.vermeg.tools.controller;

import com.vermeg.tools.model.Server;
import com.vermeg.tools.model.User;
import com.vermeg.tools.service.ServerService;
import com.vermeg.tools.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller ( "/" )
public class HomeController {

    @Autowired
    private UserService userService;

    @Autowired
    private ServerService serverService;


    @GetMapping ( value = {"/home", ""} )
    private String getHomePage() {
        return "home";
    }


    @GetMapping ( value = {"users"} )
    private ModelAndView getApplications(
            @RequestParam ( defaultValue = "1", name = "page" ) Integer pageNo,
            @RequestParam ( defaultValue = "5" ) Integer pageSize,
            @RequestParam ( defaultValue = "id" ) String sortBy) {
        ModelAndView modelAndView = new ModelAndView("users");
        Page<User> usersPage = userService.getAllUsers(pageNo > 0 ? --pageNo : 0, pageSize, sortBy);
        int totalPages = usersPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
            modelAndView.addObject("pageNumbers", pageNumbers);
        }
        modelAndView.addObject("pageNo", pageNo);
        modelAndView.addObject("totalPages", totalPages);
        modelAndView.addObject("users", usersPage.getContent());
        return modelAndView;
    }

    @GetMapping ( value = {"servers"} )
    private ModelAndView getApplicationsServer(
            @RequestParam ( defaultValue = "1", name = "page" ) Integer pageNo,
            @RequestParam ( defaultValue = "5" ) Integer pageSize,
            @RequestParam ( defaultValue = "id" ) String sortBy) {
        ModelAndView modelAndView = new ModelAndView("servers");
        Page<Server> serverPage = serverService.getAllServers(pageNo > 0 ? --pageNo : 0, pageSize, sortBy);
        int totalPages = serverPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
            modelAndView.addObject("pageNumbers", pageNumbers);
        }
        modelAndView.addObject("pageNo", pageNo);
        modelAndView.addObject("totalPages", totalPages);
        modelAndView.addObject("servers", serverPage.getContent());
        return modelAndView;
    }

    @GetMapping ( value = {"profile"} )
    private String getProfile()
    {
        return "profile";
    }





}
