<#import "/spring.ftl" as spring>
<#include "../components/header.ftl">
<div class="my-3 my-md-5">
    <div class="container">

        <div class="text-center mb-6">
            <div class="col-lg-5">
                <div class="container text-center">
                    <div class="row">
                        <div class="display-1 text-muted mb-5"><i class="si si-exclamation"></i> 500</div>
                    </div>

                    <div class="row">
                        <h1 class="h2 mb-3">Oops.. You just found an error page..</h1>
                        <p class="h4 text-muted font-weight-normal mb-7">We are sorry but our service is currently not available&hellip;We are sorry but your request contains bad syntax and cannot be fulfilled...&hellip;</p>
                    </div>
                    <div class="row">
                        <button type="submit" class="btn btn-primary  btn-sm" onclick="javascript:history.back()"><i class="fe fe-arrow-left mr-2"></i>Go back</button>
                    </div>
                </div>
            </div>
        </div>

<#include "../components/footer.ftl">