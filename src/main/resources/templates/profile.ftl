<#import "/spring.ftl" as spring>
<#include "components/header.ftl">
        <div class="my-3 my-md-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card card-profile">
                            <div class="card-header" style="background-image: url(${rc.getContextPath()}/pictures/faces/pic2.jpg)"></div>
                            <div class="card-body text-center">
                                <img class="card-profile-img" src="${rc.getContextPath()}/pictures/faces/pic1.jpeg">
                                <h3 class="mb-3">SAHBENI IHEB</h3>
                                <p class="mb-4">
                                    Trainee at Vermeg.
                                </p>
                                <button class="btn btn-outline-primary btn-sm">
                                    <span class="fa fa-twitter"></span> Follow
                                </button>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="media">
                                    <span class="avatar avatar-xxl mr-5" style="background-image: url()"></span>
                                    <div class="media-body">
                                        <h4 class="m-0">OUSSAMA CHOUAIBI</h4>
                                        <p class="text-muted mb-0">Webdeveloper</p>
                                        <ul class="social-links list-inline mb-0 mt-2">
                                            <li class="list-inline-item">
                                                <a href="javascript:void(0)" title="Facebook" data-toggle="tooltip"><i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="javascript:void(0)" title="Twitter" data-toggle="tooltip"><i class="fa fa-twitter"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="javascript:void(0)" title="1234567890" data-toggle="tooltip"><i class="fa fa-phone"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="javascript:void(0)" title="@skypename" data-toggle="tooltip"><i class="fa fa-skype"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">My Profile</h3>
                            </div>
                            <div class="card-body">
                                <form>
                                    <div class="row">
                                        <div class="col-auto">
                                            <span class="avatar avatar-xl" style="background-image: url(${rc.getContextPath()}/pictures/faces/pic1.jpeg)"></span>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label">Email-Address</label>
                                                <input class="form-control" placeholder="your-email@domain.com"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Bio</label>
                                        <textarea class="form-control" rows="5">Computer science student at EniCarthage</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Email-Address</label>
                                        <input class="form-control" placeholder="your-email@domain.com"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Password</label>
                                        <input type="password" class="form-control" value="password"/>
                                    </div>
                                    <div class="form-footer">
                                        <button class="btn btn-primary btn-block">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-header">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Message">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-secondary">
                                            <i class="fe fe-camera"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <ul class="list-group card-list-group">
                                <li class="list-group-item py-5">
                                    <div class="media">
                                        <div class="media-object avatar avatar-md mr-4" style="background-image: url(${rc.getContextPath()}/pictures/faces/pic1.jpeg)"></div>
                                        <div class="media-body">
                                            <div class="media-heading">
                                                <small class="float-right text-muted">4 min</small>
                                                <h5>SAHBENI IHEB</h5>
                                            </div>
                                            <div>
                                                Bonjour!
                                            </div>
                                        </div>
                                    </div>
                                </li>

                        <form class="card">
                            <div class="card-body">
                                <h3 class="card-title">Edit Profile</h3>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="form-label">Company</label>
                                            <input type="text" class="form-control" disabled="" placeholder="Company" >
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label class="form-label">Username</label>
                                            <input type="text" class="form-control" placeholder="Username"">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="form-group">
                                            <label class="form-label">Email address</label>
                                            <input type="email" class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">First Name</label>
                                            <input type="text" class="form-control" placeholder="Company" >
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Last Name</label>
                                            <input type="text" class="form-control" placeholder="Last Name" >
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">Address</label>
                                            <input type="text" class="form-control" placeholder="Home Address" >
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="form-group">
                                            <label class="form-label">City</label>
                                            <input type="text" class="form-control" placeholder="City" >
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label class="form-label">Postal Code</label>
                                            <input type="number" class="form-control" placeholder="ZIP Code">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="form-label">Country</label>
                                            <select class="form-control custom-select">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">About Me</label>
                                            <textarea rows="5" class="form-control" placeholder="Here can be your description" ></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary">Update Profile</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
            </div>
        </div>
    </div>
</div>
        <#include "components/footer.ftl">
