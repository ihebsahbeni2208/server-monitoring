</div>
      <footer class="footer">
          <div class="container">
              <div class="row align-items-center flex-row-reverse">
                  <div class="col-auto ml-lg-auto">
                      <div class="row align-items-center">
                          <div class="col-auto">
                              <ul class="list-inline list-inline-dots mb-0">
                                  <li class="list-inline-item"><a href="${rc.getContextPath()}/helo.html">Help</a></li>
                                  <li class="list-inline-item"><a href="${rc.getContextPath()}/faq.html">FAQ</a></li>
                              </ul>
                          </div>
                      </div>
                  </div>
                  <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
                      Copyright © 2019 <a href=".">Server Monitoring App</a> All rights reserved.
                  </div>
              </div>
          </div>
      </footer>
    </div>
  </body>
</html>