<#include "components/header.ftl">
<div class="my-3 my-md-5">
    <div class="container">
        <div class="col-1"></div>
        <div class="col-10">
            <div class="card card-scroll">
                <div class="card-header">
                    <div class="col-10">
                        <h3 class="card-title">Servers</h3>
                    </div>
                    <div class="col-1">
                        <a class="btn btn-primary btn-sm" href="${rc.getContextPath()}/server/add">
                            Add Server
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <#if isCreated?? && isCreated>
                        <div class="alert alert-success  alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"></a>
                            <strong>Success!</strong> Server created with success!
                    </#if>
                    <#if isEdited?? && isEdited>
                        <div class="alert alert-success  alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"></a>
                            <strong>Success!</strong> Server edited with success!
                        </div>
                    </#if>
                        <#if isDeleted?? && isDeleted>
                        <div class="alert alert-success  alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"></a>
                            <strong>Success!</strong> Server deleted with success!
                            </#if>
                        </div>
                    <table class="table table-responsive card-table table-center text-nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Connection type</th>
                            <th>Name</th>
                            <th>User Name</th>

                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <#if !servers?has_content>
                            <tr class="text-center">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    Emtpy Servers !
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <#else>
                            <#list servers as server>
                                <tr>
                                    <td>
                                        ${server.id}
                                    </td>
                                    <td>
                                        ${server.connectionType!}
                                    </td>
                                    <td>
                                        ${server.name!}
                                    </td>
                                    <td>
                                        ${server.userName!}
                                    </td>


                                    <td class="text-right">
                                        <div class="dropdown">
                                            <button class="btn btn-secondary btn-sm dropdown-toggle"
                                                    data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">Actions
                                            </button>
                                            <div class="dropdown-menu text-center">
                                                <a class="dropdown-item"
                                                   href="${rc.getContextPath()}/servers/${server.id}/edit">Edit</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item js-delete-server" href="javascript:void(0)"
                                                   data-toggle="modal" data-target="#deleteServer"
                                                   data-id="${server.id}">
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </#list>
                        </#if>
                        </tbody>
                    </table>
                </div>
                <#if pageNumbers?? && totalPages gt  1 >
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <#if pageNo gt  0 >
                                <li class="page-item"><a class="page-link" href="/servers?page=${pageNo-1}">Previous</a></li>
                            </#if>
                            <#list pageNumbers as pageNumber>
                                <#if (pageNo+1) == pageNumber>
                                    <li class="page-item active">
                                        <a class="page-link" href="/servers?page=${pageNumber}">${pageNumber}</a>
                                    </li>
                                <#else >
                                    <li class="page-item">
                                        <a class="page-link" href="/servers?page=${pageNumber}">${pageNumber}</a>
                                    </li>
                                </#if>
                            </#list>
                            <#if pageNo lt totalPages>
                                <li class="page-item"><a class="page-link" href="/servers?page=${pageNo+1}">Next</a></li>
                            </#if>
                        </ul>
                    </div>
                </#if>
            </div>
        </div>
        <div class="col-1"></div>
    </div>
</div>
<div id="deleteServer" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="text-center modal-body">
                <h3>Do you want to delete server?</h3>
            </div>
            <div class="modal-footer">
                <form action="${rc.getContextPath()}/server/delete" method="post">
                    <input type="hidden" id="delete-serverId" name="id" value="">
                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="submit" type="button" class="btn btn-primary">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    require(['jquery'], function ($) {
        $(function () {
            $('body').on('click', '.js-delete-server', function () {
                let serverId = $(this).attr('data-id');
                $("#delete-serverId").val(serverId);
            });
        });
    });

</script>
<#include "components/footer.ftl">