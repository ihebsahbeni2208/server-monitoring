<#include "components/header.ftl">
<div class="my-3 my-md-5">
    <div class="container">
        <div class="col-1"></div>
        <div class="col-10">
            <div class="card card-scroll">
                <div class="card-header">
                    <div class="col-10">
                        <h3 class="card-title">Users</h3>
                    </div>
                    <div class="col-1">
                        <a class="btn btn-primary btn-sm" href="${rc.getContextPath()}/user/add">
                            Add User
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div id="tests-application-result" class="invisible">
                        You successfully read this important alert message.
                    </div>
                    <div class="card-body">
                        <#if isCreated?? && isCreated>
                        <div class="alert alert-success  alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"></a>
                            <strong>Success!</strong> user created with success!
                            </#if>
                            <#if isEdited?? && isEdited>
                                <div class="alert alert-success  alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"></a>
                                    <strong>Success!</strong> user edited with success!
                                </div>
                            </#if>
                            <#if isDeleted?? && isDeleted>
                            <div class="alert alert-success  alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"></a>
                                <strong>Success!</strong> user deleted with success!
                                </#if>
                            </div>
                    <table class="table table-responsive card-table table-center text-nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Is Active</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <#if !users?has_content>
                            <tr class="text-center">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    Emtpy Users !
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <#else>
                            <#list users as user>
                                <tr>
                                    <td>
                                        ${user.id}
                                    </td>
                                    <td>
                                        ${user.userName!}
                                    </td>
                                    <td>
                                        ${user.email!}
                                    </td>
                                    <td>
                                        ${user.firstName!}
                                    </td>
                                    <td>
                                        ${user.lastName!}
                                    </td>
                                    <td>
                                        ${user.active!}
                                    </td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <button class="btn btn-secondary btn-sm dropdown-toggle"
                                                    data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">Actions
                                            </button>
                                            <div class="dropdown-menu text-center">
                                                <a class="dropdown-item"
                                                   href="${rc.getContextPath()}/users/${user.id}/edit">Edit</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item js-delete-user" href="javascript:void(0)"
                                                   data-toggle="modal" data-target="#deleteUser"
                                                   data-id="${user.id}">
                                                    Delete
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </#list>
                        </#if>
                        </tbody>
                    </table>
                </div>
                <#if pageNumbers?? && totalPages gt  1 >
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <#if pageNo gt  0 >
                                <li class="page-item"><a class="page-link" href="/users?page=${pageNo-1}">Previous</a></li>
                            </#if>
                            <#list pageNumbers as pageNumber>
                                <#if (pageNo+1) == pageNumber>
                                    <li class="page-item active">
                                        <a class="page-link" href="/users?page=${pageNumber}">${pageNumber}</a>
                                    </li>
                                <#else >
                                    <li class="page-item">
                                        <a class="page-link" href="/users?page=${pageNumber}">${pageNumber}</a>
                                    </li>
                                </#if>
                            </#list>
                            <#if pageNo lt totalPages>
                                <li class="page-item"><a class="page-link" href="/users?page=${pageNo+1}">Next</a></li>
                            </#if>
                        </ul>
                    </div>
                </#if>

            </div>
        </div>
        <div class="col-1"></div>
    </div>
</div>
<div id="deleteUser" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="text-center modal-body">
                <h3>Do you want to delete user?</h3>
            </div>
            <div class="modal-footer">
                <form action="${rc.getContextPath()}/user/delete" method="post">
                    <input type="hidden" id="delete-userId" name="id" value="">
                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="submit" type="button" class="btn btn-primary">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    require(['jquery'], function ($) {
        $(function () {
            $('body').on('click', '.js-delete-user', function () {
                let userId = $(this).attr('data-id');
                $("#delete-userId").val(userId);
            });
        });
    });

</script>
<#include "components/footer.ftl">