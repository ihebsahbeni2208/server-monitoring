<#import "/spring.ftl" as spring>
<#include "components/header.ftl">
<div class="my-3 my-md-5">
    <div class="container">
        <div class="row">
            <@spring.bind "user" />
            <div class="col-12">
                <div class="card-body">
                    <#if spring.status.error>
                        <ul>
                            <#list spring.status.errorMessages as error>
                                <h3 class=" text-center alert-danger">${error}</h3>
                            </#list>
                        </ul>
                    </#if>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <form class="card"
                                  action="${(action?? && action = 'add')?then('${rc.getContextPath()}/user/add', '${rc.getContextPath()}/user/edit')}"
                                  method="post">
                                <div class="card-body">
                                    <h3 class="card-title">User</h3>
                                    <div class="row">

                                        <div class="col-sm-6 col-md-6">
                                            <input type="hidden" name="id" class="form-control"
                                                   value="${(user??&&user.id?has_content)?then(user.id, '')}">
                                            <div class="form-group">
                                                <label class="form-label">User Name</label>
                                                <@spring.bind "user.userName" />
                                                <input type="text" name="userName" class="form-control"
                                                       value="${(user??&&user.userName?has_content)?then(user.userName, '')}"
                                                       placeholder="exp:admin">
                                                <#list spring.status.errorMessages as error>
                                                    <span class=" text-center alert-danger">${error}</span>
                                                </#list>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">User Email</label>
                                                <@spring.bind "user.email" />
                                                <input type="text" name="email" class="form-control"
                                                       value="${(user??&&user.email?has_content)?then(user.email, '')}"
                                                       placeholder="exp:test@test.com">
                                                <#list spring.status.errorMessages as error>
                                                    <span class=" text-center alert-danger">${error}</span>
                                                </#list>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">First Name</label>
                                                <@spring.bind "user.firstName" />
                                                <input type="text" name="firstName" class="form-control"
                                                       value="${(user??&&user.firstName?has_content)?then(user.firstName, '')}"
                                                       placeholder="exp:admin">
                                                <#list spring.status.errorMessages as error>
                                                    <span class=" text-center alert-danger">${error}</span>
                                                </#list>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Last Name</label>
                                                <@spring.bind "user.lastName" />
                                                <input type="text" name="lastName" class="form-control"
                                                       value="${(user??&&user.lastName?has_content)?then(user.lastName, '')}"
                                                       placeholder="exp:me">
                                                <#list spring.status.errorMessages as error>
                                                    <span class=" text-center alert-danger">${error}</span>
                                                </#list>
                                            </div>
                                        </div>
                                        <#if action?? && action = "add">
                                            <div class="col-sm-6 col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Password</label>
                                                    <@spring.bind "user.password" />
                                                    <input type="password" name="password" class="form-control"
                                                           value="${(user??&&user.password?has_content)?then(user.password, '')}">
                                                    <#list spring.status.errorMessages as error>
                                                        <span class=" text-center alert-danger">${error}</span>
                                                    </#list>
                                                </div>
                                            </div>

                                        <#else >
                                            <input type="hidden" name="password" class="form-control"
                                                   value="${(user??&&user.password?has_content)?then(user.password, '')}">
                                        </#if>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <a href="${rc.getContextPath()}/" class="btn btn-secondary">Cancel</a>
                                    <button type="submit" class="btn btn-primary">Save User</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<#include "components/footer.ftl">