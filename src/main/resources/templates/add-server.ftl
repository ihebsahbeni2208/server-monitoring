<#import "/spring.ftl" as spring>
<#include "components/header.ftl">
<div class="my-3 my-md-5">
    <div class="container">
        <div class="row">
            <@spring.bind "serverData" />
            <div class="col-12">
                <div class="card-body">
                    <#if spring.status.error>
                        <ul>
                            <#list spring.status.errorMessages as error>
                                <h3 class=" text-center alert-danger">${error}</h3>
                            </#list>
                        </ul>
                    </#if>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <form class="card"
                                  action="${(action?? && action = 'add')?then('${rc.getContextPath()}/server/add', '${rc.getContextPath()}/server/edit')}"
                                  method="post">

                                <div class="card-body">
                                    <h3 class="card-title">Server</h3>
                                    <div class="row">
                                        <input type="hidden" name="id" class="form-control"
                                               value="${(serverData??&&serverData.id?has_content)?then(serverData.id, '')}">
                                            <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Server Name</label>
                                                <@spring.bind "serverData.name" />
                                                <input type="text" name="name" class="form-control"
                                                       value="${(serverData??&&serverData.name?has_content)?then(serverData.name, '')}"
                                                       placeholder="exp:admin">
                                                <#list spring.status.errorMessages as error>
                                                    <span class=" text-center alert-danger">${error}</span>
                                                </#list>
                                            </div>
                                        </div>


                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">User Name</label>
                                                <@spring.bind "serverData.userName" />
                                                <input type="text" name="userName" class="form-control"
                                                       value="${(serverData??&&serverData.userName?has_content)?then(serverData.userName, '')}"
                                                       placeholder="user user">
                                                <#list spring.status.errorMessages as error>
                                                    <span class=" text-center alert-danger">${error}</span>
                                                </#list>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Connection Type</label>
                                                <@spring.bind "serverData.connectionType" />
                                                <input type="text" name="connectionType" class="form-control"
                                                       value="${(serverData??&&serverData.connectionType?has_content)?then(serverData.connectionType, '')}"
                                                       placeholder="exp:SSH/FTP">
                                                <#list spring.status.errorMessages as error>
                                                    <span class=" text-center alert-danger">${error}</span>
                                                </#list>
                                            </div>
                                        </div>

                                        <#if action?? && action = "add">
                                            <div class="col-sm-6 col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Password</label>
                                                    <@spring.bind "serverData.password" />
                                                    <input type="password" name="password" class="form-control"
                                                           value="${(serverData??&&serverData.password?has_content)?then(serverData.password, '')}">
                                                    <#list spring.status.errorMessages as error>
                                                        <span class=" text-center alert-danger">${error}</span>
                                                    </#list>
                                                </div>
                                            </div>
                                        <#else >
                                            <input type="hidden" name="password" class="form-control"
                                                   value="${(serverData??&&serverData.password?has_content)?then(serverData.password, '')}">
                                        </#if>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <a href="${rc.getContextPath()}/" class="btn btn-secondary">Cancel</a>
                                    <button type="submit" class="btn btn-primary">Save Server</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<#include "components/footer.ftl">