
--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`
(
    `user_id`       bigint(20) NOT NULL AUTO_INCREMENT,
    `active`        int(11)      DEFAULT NULL,
    `creation_date` datetime     DEFAULT NULL,
    `email`         varchar(255) DEFAULT NULL,
    `first_name`    varchar(255) DEFAULT NULL,
    `last_name`     varchar(255) DEFAULT NULL,
    `password`      varchar(255) DEFAULT NULL,
    `user_name`     varchar(255) DEFAULT NULL,
    PRIMARY KEY (`user_id`)
)ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`
(
    `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `role`    varchar(255) DEFAULT NULL,
    PRIMARY KEY (`role_id`)
)ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`
(
    `user_id` bigint(20) NOT NULL,
    `role_id` bigint(20) NOT NULL,
    PRIMARY KEY (`user_id`, `role_id`),
    CONSTRAINT `FK_user-role_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
    CONSTRAINT `FK_user-role_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
)ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE `notification`
(
    `notification_id`   bigint(20)  NOT NULL AUTO_INCREMENT,
    `creation_date`     datetime     DEFAULT NULL,
    `notification_mssg` varchar(255) DEFAULT NULL,
    `notification_type` varchar(20) NOT NULL,
    `user_id`           bigint(20)  NOT NULL,
    PRIMARY KEY (`notification_id`),
    CONSTRAINT `FK_notification_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),

)ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


--
-- Table structure for table `server`
--

DROP TABLE IF EXISTS `server`;
CREATE TABLE `server`
(
    `server_id`       bigint(20) NOT NULL AUTO_INCREMENT,
    `connection_type` varchar(255) DEFAULT NULL,
    `name`            varchar(255) DEFAULT NULL,
    `password`        varchar(255) DEFAULT NULL,
    `user_name`       varchar(255) DEFAULT NULL,
    `user_id`         bigint(20) NOT NULL,
    PRIMARY KEY (`server_id`),
    CONSTRAINT `FK_server_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),

)ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Table structure for table `notification`
--

CREATE TABLE `monitoring`
(
    `monitoring_id`   bigint(20) NOT NULL AUTO_INCREMENT,
    `message`         varchar(255) DEFAULT NULL,
    `message_type`    varchar(255) DEFAULT NULL,
    `time`            datetime     DEFAULT NULL,
    `server_id` bigint(20) NOT NULL,
    PRIMARY KEY (`monitoring_id`),
    CONSTRAINT `FK_monitoring_server` FOREIGN KEY (`server_id`) REFERENCES `server` (`server_id`),

)ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

