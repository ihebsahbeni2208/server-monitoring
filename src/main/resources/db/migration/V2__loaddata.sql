INSERT INTO role (role_id, role) VALUES (1, 'ADMIN');
INSERT INTO role (role_id, role) VALUES (2, 'USER');
INSERT INTO user (user_id, active, email, first_name, last_name, user_name, password)
VALUES (1, 1, 'root@root.com', 'Root', 'Me', 'root', '$2a$10$Ws5fhEIg5HVj1gAszhkZj.TpXAT30n0LXUIlwn1Z7Z0OcjyVkFjBS');
INSERT INTO user_role (user_id, role_id) VALUES (1, 1);