class TestManager {
    constructor(applicationUuid, ctx) {
        this.applicationUuid = applicationUuid;
        this.ctx = ctx;
        this.sortableElement = null;
        this.initSortable();
        this.getTestMethodsUtil();
        this.initConstants();
        this.$testcasetable = $("#testcaseTable");
        this.$testcaseForm = $('#testcase-form');
        this.$testmethodForm = $('#testmethod-form');

        this.initCallback();
    }


    static replaseStr(string, obj) {
        if (typeof obj === 'string') {
            let re = new RegExp('{}', 'g');
            string = string.replace(re, obj);
        } else {
            for (let k in obj) {
                console.log(k, obj[k]);
                let re = new RegExp('{' + k + '}', 'g');
                string = string.replace(re, obj[k]);
            }
        }
        return string;
    }

    static findEement(jsonArray, key, value) {
        return jsonArray != undefined ? jsonArray.find(function (e) {
            return e[key] == value;
        }) : null;
    }

    loadElement(source, target, url, calback) {
        let self = this;
        $(source).load(url + ' ' + target, function () {
          self.initSortable();
            if ($.isFunction(calback)) {
                calback();
            }
        });
    }

    getTestCases() {
        let self = this;
        $.get(self.ctx+ '/api/v1/test/application/' + self.applicationUuid + '/case/get/list',
            {
                page: 1,
                limit: 5
            },
            function (data, status) {
                console.log("Data: " + data + "Status: " + status);
            });
    }

    getTestCase(id) {
        let self = this;
        $.get(self.ctx+ '/api/v1/test/application/' + self.applicationUuid + '/case/get',
            {
                id: id
            },
            function (data, status) {
                console.log("Data: " + data + "Status: " + status);
                $('#testType').val(data.testType).change();
                $('#name').val(data.name);
                $('#testcase-id').val(data.id);
                $('#testcase').modal("show");
            });
    }

    saveTestCase(testcase) {
        let self = this;
        $.ajax({
            type: 'POST',
            url: self.ctx+ '/api/v1/test/application/' + self.applicationUuid + '/case/save',
            data: JSON.stringify(testcase),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }).done(function (data) {
            if (self.$testcasetable.find("#testcase-" + data.id).length > 0) {
                self.$testcasetable.find("#testcase-" + data.id).find('.js-testcase-name').text(data.name);
                self.$testcasetable.find("#testcase-" + data.id).find('.js-testcase-testType').text(data.testType);


            } else {
                let source = '.js-testcaseTable';
                let target = '#testcaseTable';
                self.loadElement(source, target, self.ctx+ '/manage-test?application=' + self.applicationUuid, null);
            }
            self.$testcasetable.find("#testcase-" + data.id).addClass('bg-teal');
            setTimeout(function () {
                $("#testcaseTable").find('.bg-teal').removeClass('bg-teal');
            }, 3000);
            $('#testcase').modal("hide");
            self.initTestcaseForm();
        }).fail(function (jqXHR, textStatus) {
            console.log(textStatus)
        });
    }

    removeTestCase(id) {
        let self = this;
        $.ajax({
            type: 'DELETE',
            url: self.ctx+ '/api/v1/test/application/case/delete/' + id,
            contentType: "application/json; charset=utf-8",
            dataType: 'json'
        }).done(function (data) {
            self.$testcasetable.find('#testcase-' + data.code).remove();
            self.$testcasetable.find("tr[data-testcaseId='"+ data.code +"']").remove();
            $('#deletetestcase-id').val('');
            $('#deletetestcase').modal("hide");
        });
    }

    runTestCase(id) {
        let self = this;
        $.ajax({
            type: 'POST',
            url: self.ctx+ '/api/v1/test/application/case/' + id + '/run',
            contentType: "application/json; charset=utf-8",
            dataType: 'json'
        }).done(function (data) {
            let mssg = "Respons: {" +
                "testName=" + data.name +
                ", runCount=" + data.runCount +
                ", failureCount=" + data.failureCount +
                ", ignoreCount=" + data.ignoreCount +
                ", runTime=" + data.runTime +
                ", failures=" + data.failures +
                '}';
            $('#testcase-result').empty();
            $('#testcase-result').text(mssg);
            $('#testcase-result').removeClass('alert-success');
            $('#testcase-result').removeClass('alert-danger');
            $('.js-run-testcase').removeClass('text-green');
            $('.js-run-testcase').find('i').attr('class', 'fa fa-play-circle-o fa-lg');
            if (data.failureCount == 0) {
                $('#testcase-result').addClass('alert-success');
            } else {
                $('#testcase-result').addClass('alert-danger');
            }
            $('#testcase-result').removeClass('invisible');

        });
    }

    saveTestMethod(testMethod, testcaseId) {
        let self = this;
        let testM = TestManager.findEement(this.testMethodsUtil, 'name', testMethod.name);
        testMethod.className = testM.className;
        testMethod.returnTypeClass = testM.returnTypeClass;
        $.ajax({
            type: 'POST',
            url: self.ctx+ '/api/v1/test/application/case/' + testcaseId + '/method/save',
            data: JSON.stringify(testMethod),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }).done(function (data) {
            let source = '[data-testcaseid="' + data.testCaseId + '"].js-testcase-testmethod td:first';
            let target = '[data-testcaseid="' + data.testCaseId + '"].js-testcase-testmethod div#testcase-method-' + data.testCaseId ;
            self.loadElement(source, target, self.ctx+ '/manage-test?application=' + self.applicationUuid, function () {
                $('#testcase-' + data.testCaseId)[0].click();
            });

            $('#testmethod').modal("hide");
            self.initTestcaseForm();
        }).fail(function (jqXHR, textStatus) {
            console.log(textStatus)
        });
    }

    removeTestMethod(id) {
        let self = this;
        $.ajax({
            type: 'DELETE',
            url: self.ctx+ '/api/v1/test/application/case/method/delete/' + id,
            contentType: "application/json; charset=utf-8",
            dataType: 'json'

        }).done(function (data) {
            $('.js-testmethod').find('#testMethod-' + data.code).parents('li').remove();
            $('#deletetestcase-id').val('');
            $('#deletetestmethod').modal("hide");
        }).fail(function (jqXHR, textStatus) {
            console.log(textStatus)
        });
    }

    orderTestCaseMethods(testcaseId, data) {
        let self = this;
        $.ajax({
            type: 'POST',
            url: self.ctx+ '/api/v1/test/application/case/' + testcaseId + '/method/orders',
            data: JSON.stringify({'orders': data}),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }).done(function (data) {
            console.log(data);
        }).fail(function (jqXHR, textStatus) {
            console.log(textStatus)
        });
    }

    getTestMethodsUtil() {
        let self = this;
        $.get(self.ctx+ '/api/v1/test/method/get', function (data, status) {
            self.testMethodsUtil = data;
        });
    }

    initTestcaseForm() {
        this.$testcaseForm.find("#testcase-ranking").val('');
        this.$testcaseForm.find("#testcase-id").val('');
        this.$testcaseForm.find("#name").val('');
        this.$testcaseForm.find("#testType").val("IHM").change();
    }

    initTestMethodForm(id, testcaseId, name) {
        this.$testmethodForm.find("#testmethod-id").val(id);
        this.$testmethodForm.find("#testmethod-testcaseId").val(testcaseId);
        this.$testmethodForm.find("#testmethod-name").val(name).change();
        this.$testmethodForm.find('#testmethod-parameters').empty();
        //this.$testmethodForm.find("#testmethod-name").trigger("change");
        $('#testmethod-name').trigger('chosen:updated');
    }

    initTestMethodParameters(parameters) {
        if (parameters && typeof parameters == "object") {
            for (let i = 0; i < parameters.length; i++) {
                let parent = $('input[value="' + parameters[i].name + '"]').closest('.form-group');
                parent.find('[name*="\[value\]"]').val(parameters[i].value);
                parent.find('[name*="\[id\]"]').val(parameters[i].id);

            }
        }
    }


    initMethodUtility(testType) {
        this.$testmethodForm.find('#testmethod-name').empty();
        for (let i = 0; i < this.testMethodsUtil.length; i++) {
            if (testType == this.testMethodsUtil[i].testType) {
                let option = '<option value="' + this.testMethodsUtil[i].name + '">' + this.testMethodsUtil[i].name + '</option>';
                this.$testmethodForm.find('#testmethod-name').append(option);
            }
        }
        this.$testmethodForm.find('#testmethod-name').change();
        $('#testmethod-name').trigger('chosen:updated');
    }

    initSortable(){
        let self = this;
        self.sortableElement = $("ol.js-testmethod").sortable({
            group: 'no-drop',
            handle: 'i.fa-arrows',
            onDragStart: function ($item, container, _super) {
                if (!container.options.drop)
                    $item.clone().insertAfter($item);
                _super($item, container);
            },
            onDrop: function ($item, container, _super) {
                let index = $("ol.js-testmethod").index(container.target);
                let data = self.sortableElement.sortable("serialize").get(index);
                let testcaseId = $(container.target).attr('data-testcaseId');
                for (let i = 0; i < data.length; i++) {
                    let item = data[i];
                    let $child = $(container.target).find('li#testcase-' + testcaseId + '_testmethod-' + item.methodid);
                    item.ranking = i + 1;
                    $child.attr('data-ranking', item.ranking);
                }
                self.orderTestCaseMethods(testcaseId, data);
                _super($item, container);
            }
        });
    }
    initCallback() {

        $(".chosen-select").chosen({
            placeholder_text_single : 'select a method'
        });
        let self = this;
        $('body').delegate(".js-add-testcase", "click", function (event) {
            self.initTestcaseForm();
            let ranking = $(this).attr('data-ranking');
            $('#testcase-ranking').val(ranking);
            $('#testcase').modal("show");
            event.preventDefault();
        });

        $('body').delegate("#testcase-form", "submit", function (event) {
            let testcase = $(this).serializeForm();
            self.saveTestCase(testcase);
            event.preventDefault();
        });

        $('body').delegate(".js-edit-testcase", "click", function (event) {
            let $testcase = $(this).parents('.js-testcase'),
                id = $testcase.find('.js-testcase-id').text(),
                name = $testcase.find('.js-testcase-name').text(),
                testType = $testcase.find('.js-testcase-testType').text();

            self.$testcaseForm.find("#testcase-id").val(id);
            self.$testcaseForm.find("#testType").val(testType).change();
            self.$testcaseForm.find("#name").val(name);
            $('#testcase').modal("show");
            $(".dropdown-toggle[aria-expanded='true']").dropdown("toggle");
            event.stopPropagation();
        });

        $('body').delegate(".js-delete-testcase", "click", function (event) {
            let $testcase = $(this).parents('.js-testcase'),
                id = $testcase.find('.js-testcase-id').text();
            $('#deletetestcase-id').val(id);
            $('#deletetestcase').modal("show");
            $(".dropdown-toggle[aria-expanded='true']").dropdown("toggle");
            event.stopPropagation();
        });

        $('body').delegate("#deletetestcase-form", "submit", function (event) {
            let id = $(this).find('#deletetestcase-id').val();
            self.removeTestCase(id);
            event.preventDefault();
        });


        $('body').delegate("#testmethod-form #testmethod-name", "change", function (event) {
            self.$testmethodForm.find('#testmethod-parameters').empty();
            let functionStr = $(this).val();
            let obj = TestManager.findEement(self.testMethodsUtil, 'name', functionStr);
            if (obj && obj.testParameters) {
                for (let i = 0; i < obj.testParameters.length; i++) {
                    self.$testmethodForm.find('#testmethod-parameters')
                        .append(TestManager.replaseStr(self.methodParam,
                            {
                                'testParam-name': obj.testParameters[i].name,
                                'testParam-id': "",
                                'testParam-index': i,
                            }))
                }
            }
        });

        $('body').delegate(".js-add-testmethod", "click", function (event) {
            let testcaseId = $(this).parents('.js-testcase-testmethod').attr('data-testcaseId');
            let testType = $("#testcase-" + testcaseId).find('.js-testcase-testType').text();
            self.initMethodUtility(testType);
            self.initTestMethodForm("", "", "");
            self.$testmethodForm.find("#testmethod-testcaseId").val(testcaseId);
            $('#testmethod').modal("show");
            event.preventDefault();
        });

        $('body').delegate("#testmethod-form", "submit", function (event) {
            let testTYpe = $(this).attr('js-testcase-testType');
            let testmethod = $(this).serializeForm();
            let testcaseId = $(this).find('#testmethod-testcaseId').val();
            self.saveTestMethod(testmethod, testcaseId);
            event.preventDefault();
        });

        $('body').delegate(".js-edit-testmethod", "click", function (event) {
            let testcaseId = $(this).parents('.js-testcase-testmethod').attr('data-testcaseId');
            let testType = $("#testcase-" + testcaseId).find('.js-testcase-testType').text();
            self.initMethodUtility(testType);

            let id = $(this).closest('tr').find('.js-testmethod-id').val();
            let name = $(this).closest('tr').find('.js-testmethod-name').text();
            self.initTestMethodForm(id, testcaseId, name);


            let parameters = [];

            $(this).closest('tr').find('.js-testparameter').toArray().forEach(function (element) {
                let resutl = {};
                resutl['id'] = $(element).attr('data-id');
                resutl['name'] = $(element).attr('data-name');
                resutl['value'] = $(element).text();
                parameters.push(resutl);
            });

            console.log(parameters);

            self.initTestMethodParameters(parameters);
            $('#testmethod').modal("show");
            event.preventDefault();
        });


        $('body').delegate(".js-delete-testmethod", "click", function (event) {
            let id = $(this).attr('data-id');
            $('#deletetestmethod-id').val(id);
            $('#deletetestmethod').modal("show");
            //$(".dropdown-menu.show").dropdown("toggle");
            event.preventDefault();
        });

        $('body').delegate("#deletetestmethod-form", "submit", function (event) {
            let id = $(this).find('#deletetestmethod-id').val();
            self.removeTestMethod(id);
            event.preventDefault();
        });


        $('body').delegate(".js-testcase-name", "click", function (event) {
            event.stopPropagation();
        });


        $('body').delegate(".js-run-testcase", "click", function (event) {
            let $testcase = $(this).parents('.js-testcase'),
                id = $testcase.find('.js-testcase-id').text();
            $(this).addClass('text-green');
            $(this).find('i').attr('class', 'fa fa-circle-o-notch fa-spin fa-lg');

            self.runTestCase(id);
            event.stopPropagation();
        });
    }

    initConstants() {
        this.methodParam =
            '<div class="col-md-12"><div class="form-group"><label class="form-label">{testParam-name}*</label>' +
            '<input type="hidden" name="testParameters[{testParam-index}][id]" value="{testParam-id}">' +
            '<input type="hidden" name="testParameters[{testParam-index}][name]" value="{testParam-name}">' +
            '<input type="text" name="testParameters[{testParam-index}][value]" class="form-control" placeholder="..">' +
            '</div></div>';
    }

}